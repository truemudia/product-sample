﻿import * as React from "react";

export interface IToggleSwitchProps {
    leftLabel: string;
    rightLabel: string;
    value?: boolean;
    className?: string;
    handleChangeValue(e): void;
}

export var ToggleSwitchComponent: (props: IToggleSwitchProps) => JSX.Element = props => {
    return (
        <div className={`toggle-switch-component ${props.className ? props.className : ""}`}>
            <label>
                <input type="checkbox" checked={props.value || false} onChange={(e) => props.handleChangeValue(e)} />
                <span className="slider-ball"></span>
                <span className="slider-text display-table">
                    <span className="left-slider-text display-table-cell">
                        {
                            props.leftLabel
                        }
                    </span>
                    <span className="right-slider-text display-table-cell">
                        {
                            props.rightLabel
                        }
                    </span>
                </span>
            </label>
        </div>
    );
}