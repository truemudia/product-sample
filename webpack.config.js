var Webpack = require("webpack");
var ExtractTextWebpackPlugin = require("extract-text-webpack-plugin");
var WebpackMerge = require("webpack-merge");
var WebpackCleanupPlugin = require("webpack-cleanup-plugin");

var DefaultConfig = [
    // ------------------------------------------------------
    // Main bundles
    // ------------------------------------------------------
    {
        entry: [

            // -------------------------------------------- 
            // TS 
            // -------------------------------------------- 

            "./Scripts/shared/JqueryPlugins.ts",

            "./Scripts/controllers/BannerController.ts",
            "./Scripts/controllers/CheckoutConfirmationController.ts",
            "./Scripts/controllers/CraftsmanshipController.ts",
            "./Scripts/controllers/DesignController.ts",
            "./Scripts/controllers/FactoriesController.ts",
            "./Scripts/controllers/HomeController.ts",
            "./Scripts/controllers/LensesOpticsController.ts",
            "./Scripts/controllers/LensesSunglassesController.ts",
            "./Scripts/controllers/MenuDesktopController.ts",
            "./Scripts/controllers/MenuMobileController.ts",
            "./Scripts/controllers/CheckoutPaymentController.ts",
            "./Scripts/controllers/RegisterController.ts",
            "./Scripts/controllers/WeekenderController.ts",
            "./Scripts/controllers/ShoesController.ts",

            "./Scripts/App.ts",

            // -------------------------------------------- 
            // LESS
            // -------------------------------------------- 

            "./Content/css/style.less"

        ],
        output: {
            path: "./Scripts/bundles/",
            publicPath: "/Scripts/bundles/",
            filename: "main.js",
            chunkFilename: "[chunkhash].chunk.js"
        },
        devtool: "source-map",
        resolve: {
            extensions: [".Webpack.js", ".web.js", ".less", ".css", ".ts", ".tsx", ".js"]
        },
        module: {
            rules: [
                {
                    test: /\.less|\.css$/,
                    use: ExtractTextWebpackPlugin.extract({
                        use: ["css-loader", "less-loader"]
                    })
                },
                {
                    test: /\.(png|woff|woff2|eot|otf|ttf|svg)$/,
                    use: [
                        {
                            loader: "url-loader?limit=10000&name=../../[path][name].[ext]&emitFile=false"
                        }
                    ]
                },
                {
                    test: /\.ts|\.tsx$/,
                    exclude: /(node_modules|bower_components)/,
                    use: [
                        {
                            loader: "string-replace-loader",
                            options: {
                                search: /\_import\(/g,
                                replace: "import("
                            }
                        },
                        {
                            loader: "ts-loader"
                        }
                    ]
                },
                {
                    test: /\.js$/,
                    use: [
                        {
                            loader: "source-map-loader"
                        }
                    ]
                }
            ]
        },
        plugins: [
            new ExtractTextWebpackPlugin({
                filename: "style.css"
            }),
            new WebpackCleanupPlugin({
                quiet: true
            }),
            new Webpack.ProvidePlugin({
                $: "jquery",
                jquery: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery",
                Cookies: "js-cookie",
                NProgress: "nprogress"
            })
        ]
    },
    // ------------------------------------------------------
    // Admin area bundles
    // ------------------------------------------------------
    {
        entry: [

            // -------------------------------------------- 
            // TS 
            // -------------------------------------------- 

            "./Areas/Admin/Scripts/components/EditOrder/EditOrderComponentOverride.tsx",

            // -------------------------------------------- 
            // LESS
            // -------------------------------------------- 

            "./Areas/Admin/Content/css/admin.custom.less"

        ],
        output: {
            filename: "./Areas/Admin/Scripts/bundles/admin.bundle.custom.js"
        },
        resolve: {
            extensions: [".Webpack.js", ".web.js", ".less", ".css", ".ts", ".tsx", ".js"]
        },
        module: {
            rules: [
                {
                    test: /\.less|\.css$/,
                    use: ExtractTextWebpackPlugin.extract({
                        use: ["css-loader", "less-loader"]
                    })
                },
                {
                    test: /\.(png|woff|woff2|eot|otf|ttf|svg)$/,
                    use: [
                        {
                            loader: "url-loader?limit=10000&name=../../../../[path][name].[ext]&emitFile=false"
                        }
                    ]
                },
                {
                    test: /\.ts|\.tsx$/,
                    exclude: /(node_modules|bower_components)/,
                    use: [
                        {
                            loader: "ts-loader"
                        }
                    ]
                }
            ]
        },
        plugins: [
            new ExtractTextWebpackPlugin({
                filename: "/Areas/Admin/Scripts/bundles/admin.custom.css"
            }),
            new Webpack.ProvidePlugin({
                $: "jquery",
                jquery: "jquery",
                jQuery: "jquery",
                "window.jQuery": "jquery",
                Cookies: "js-cookie",
                NProgress: "nprogress"
            })
        ]
    }
];

module.exports = function () {

    if (process.env.NODE_ENV === "production") {

        console.log("Generating production bundles...");

        var minifyConfig = {
            plugins: [
                new Webpack.DefinePlugin({
                    'process.env': {
                        NODE_ENV: JSON.stringify("production")
                    }
                }),
                new Webpack.LoaderOptionsPlugin({
                    minimize: true,
                    debug: false
                }),
                new Webpack.optimize.UglifyJsPlugin({
                    beautify: false,
                    mangle: {
                        screw_ie8: true,
                        keep_fnames: true
                    },
                    compress: {
                        screw_ie8: true
                    },
                    comments: false
                })
            ]
        };

        return [WebpackMerge(DefaultConfig[0], minifyConfig), WebpackMerge(DefaultConfig[1], minifyConfig)];
    }

    return DefaultConfig;
};