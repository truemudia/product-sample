﻿import * as React from "react";
import * as Rellax from "rellax";
import * as sumoJS from "../../sumoJS";
import * as sumoEnhancedEcommerce from "../../sumoenhancedecommerce";

import { CartItemBaseComponent } from "../CartItemBase/CartItemBaseComponent";
import { ModalComponent, IModalComponentProps } from "../Modal/ModalComponent";
import { VariantUrlToken } from "../../constants/VariantUrlToken";
import { VariantOptionName } from "../../constants/VariantOptionName";
import { VariantName } from "../../constants/VariantName";
import { ContentSection } from "../../classes/ContentSection";
import { EcommerceCartItem } from "../../classes/EcommerceCartItem";
import { EcommerceCart } from "../../classes/EcommerceCart";
import { Country } from "../../classes/Country";
import { EcommerceProductImage } from "../../classes/EcommerceProductImage";
import { EcommerceShippingBox } from "../../classes/EcommerceShippingBox";
import { ContentSectionOptions } from "../../enums/ContentSectionOptions";
import { Functions } from "../../shared/Functions";
import { CategoryName } from "../../constants/CategoryName";
import { Url } from "../../constants/Url";
import { EcommerceVariantOption } from "../../classes/EcommerceVariantOption";
import { VariantOptionUrlToken } from "../../constants/VariantOptionUrlToken";
import { CountryLanguageCode } from "../../constants/CountryLanguageCode";
import { ShippingBoxName } from "../../constants/ShippingBoxName";
import { ProductImageName } from "../../constants/ProductImageName";
import { AttributeName } from "../../constants/AttributeName";
import { ContentSectionName } from "../../constants/ContentSectionName";
import ContactFormComponent from "../ContactForm/ContactFormComponent";
import { ToggleSwitchComponent, IToggleSwitchProps } from "./reusable-components/toggleSwitchComponent";

interface IProductPageComponentState {
    country: Country;
    contentSections: Array<ContentSection>;
    cartItem: EcommerceCartItem;
    cart: EcommerceCart;
    showHtoColorSelector: boolean;
    showPanelColorSelector: boolean;
    showPanelLensesSizeSetSelector: boolean;
    isShowingMoreLensTypeOptions: boolean;
    showSizeGuideInchesTable: boolean;
}

enum StockStatus {
    InStock = 0,
    OutOfStockPreorder = 1,
    OutOfStock = 2,
    ComingSoonPreorder = 3,
    ComingSoon = 4
}

enum CustomisePanelType {
    ProductColor = 0,
    EyeglassLens = 1,
    SunglassLens = 2,
    Size = 3
}

export default class ProductPageComponent extends CartItemBaseComponent<undefined, IProductPageComponentState> {

    constructor() {
        super();

        this.state = {
            contentSections: new Array<ContentSection>()
        } as IProductPageComponentState;
    }

    // -----------------------------------------------------------------
    // ContentSections
    // -----------------------------------------------------------------

    contentSectionNames = [
        ContentSectionName.product_Eyewear_LearnMore_Link,
        ContentSectionName.product_Eyewear_Warnings_HtoContainsSameDesign,
        ContentSectionName.product_Eyewear_Warnings_HtoFull,
        ContentSectionName.product_Eyewear_Warnings_HtoOutOfStock,
        ContentSectionName.product_InformationBar_Delivery_Title,
        ContentSectionName.product_InformationBar_NeedHelp_Title,
        ContentSectionName.product_InformationBar_NeedHelp_BusinessHours,
        ContentSectionName.product_InformationBar_NeedHelp_Body,
        ContentSectionName.product_InformationBar_NeedHelp_Faq_Link,
        ContentSectionName.product_InformationBar_Returns_Title,
        ContentSectionName.product_InformationBar_ReturnsPolicy,
        ContentSectionName.product_InformationBar_Close,
        ContentSectionName.product_LensColor_Title,
        ContentSectionName.product_Lenses_Caption,
        ContentSectionName.product_Lenses_AlternativeEyewear_Title,
        ContentSectionName.product_Lenses_AlternativeEyewear_SubTitle,
        ContentSectionName.product_Lenses_Reglazing_Link,
        ContentSectionName.product_LensType_Title,
        ContentSectionName.product_LensType_MoreOptions,
        ContentSectionName.product_LensType_LessOptions,
        ContentSectionName.product_SizeSelector_SizeGuide,
        ContentSectionName.product_Specs_DetailsAndCare,
        ContentSectionName.product_Specs_NeedHelp_Caption,
        ContentSectionName.product_ActionBar_Color,
        ContentSectionName.product_ActionBar_Color_Title,
        ContentSectionName.product_ActionBar_Set,
        ContentSectionName.product_ActionBar_Set_Title,
        ContentSectionName.product_ActionBar_Size,
        ContentSectionName.product_ActionBar_Size_Title,
        ContentSectionName.product_Modals_Title,
        ContentSectionName.product_Modals_ReviewCart,
        ContentSectionName.product_Modals_BrowseTheCollection,
        ContentSectionName.product_Modals_Close,
        ContentSectionName.product_StickerWarning_OutOfStock,
        ContentSectionName.product_StickerWarning_OutOfStock_PreOrder,
        ContentSectionName.product_StickerWarning_PreOrder,
        ContentSectionName.product_CompareAtPrice,
        ContentSectionName.product_TryBeforeYouBuy,
        ContentSectionName.product_AddToCart,
        ContentSectionName.product_OutOfStock,
        ContentSectionName.shared_ShopNow,
        ContentSectionName.shared_Sunglasses,
        ContentSectionName.shared_WorkingDays,
        ContentSectionName.shared_Lenses,
        ContentSectionName.shared_Optics,
        ContentSectionName.shared_PreorderNow,
        ContentSectionName.office_Email,
        ContentSectionName.office_Phone,
        ContentSectionName.office_WorkingHours
    ];

    // -----------------------------------------------------------------
    // Component lifecycle
    // -----------------------------------------------------------------

    componentDidMount() {
        const urlToken = Functions.getUrlParameter(3);
        const queryString = decodeURIComponent(window.location.search.substring(1));
        const parameters = queryString.split("&");

        // -----------------------------------------------------------------
        // API calls
        // -----------------------------------------------------------------

        const cartPromise = sumoJS.getCart();
        const countryPromise = sumoJS.getCurrentCountry();
        const contentSectionsPromise = sumoJS.getContentSections(this.contentSectionNames);
        const cartItemPromise = sumoJS.productToCartItem(urlToken);

        // -----------------------------------------------------------------
        // Callbacks
        // -----------------------------------------------------------------

        cartPromise.done((cart) => this.setState({ cart }));

        $.when<any>(
            countryPromise,
            contentSectionsPromise,
            cartItemPromise
        ).done((
            country: Country,
            contentSections: ContentSection[],
            cartItem: EcommerceCartItem
        ) => {
            this.setState(
                {
                    country,
                    contentSections,
                    cartItem,
                    showPanelLensesSizeSetSelector: !($(window).width() > 992)
                },
                () => {

                    // -----------------------------------------------------------------
                    // Set VariantOptionValues from the url query string parameters
                    // -----------------------------------------------------------------

                    for (let i = 0; i < parameters.length; i++) {
                        const parameterName = parameters[i].split("=")[0]; // color
                        const parameterValue = parameters[i].split("=")[1]; // blue
                        const cartItemVariant = this.state.cartItem.getCartItemVariantByUrlToken(parameterName);
                        if (cartItemVariant) {
                            this.setVariantOptionValueByUrlToken(cartItemVariant.variant.name, parameterValue);
                        }
                    }

                    // -----------------------------------------------------------------
                    // Set Eyewear default values
                    // -----------------------------------------------------------------

                    if (this.getProductCategories().some(x => x.name === CategoryName.eyewear)) {

                        this.setVariantOptionValue(VariantName.prescriptionType, null); // prescription will be set in the customization page
                        this.setVariantOptionValueByName(VariantName.eyewearStockBatch, VariantOptionName.eyewearStockBatch_Global);
                        this.setVariantOptionValueByName(VariantName.prescriptionStrength, VariantOptionName.prescriptionStrength_Normal);

                        if (this.getLensColorValue(true).name === VariantOptionName.lensColor_Clear) {
                            this.setVariantOptionValueByName(VariantName.lensType, VariantOptionName.lensType_Index167Prescription);
                        } else {
                            this.setVariantOptionValueByName(VariantName.lensType, VariantOptionName.lensType_SunIndex150Polarized);
                        }
                    }

                    // -----------------------------------------------------------------
                    // Break title nicely
                    // -----------------------------------------------------------------

                    $(".header h1").prettyBreak();

                    // -----------------------------------------------------------------
                    // Instantiate product carousel & set product images
                    // -----------------------------------------------------------------

                    $(".product-carousel").owlCarousel({
                        items: 1,
                        mouseDrag: true,
                        slideBy: "page" // required for "to.owl.carousel" event to work
                    });

                    this.updateProductImages();

                    // ----------------------------------------------------------
                    // Information bar
                    // ----------------------------------------------------------

                    $(".information-bar .header .item").on("click", function () {
                        const $body = $(".information-bar > .body");

                        $body.removeClass("hidden");

                        $(".information-bar > .body > .content").hide();
                        $body.show();

                        const target = $(this).data("target");
                        $body.find(`.${target}`).show();
                    });

                    // ----------------------------------------------------------
                    // Initialize Page Animations
                    // ----------------------------------------------------------

                    this.initializeAnimations();

                    // ----------------------------------------------------------
                    // Initialize parallax
                    // ----------------------------------------------------------

                    this.initializeParallax();

                    $("#size-chart table").addClass("table");
                }
            );

            sumoEnhancedEcommerce.productDetail(cartItem, country.iso4217CurrencySymbol);
        });

        $(document).click(() => {
            if (!$(event.target).closest('.customise-panel').length && !$(event.target).is('.action-bar-color, .action-bar-option, .try')) {
                this.handleCloseCustomisePanel();
            }
        });
    }

    componentDidUpdate() {

        this.updateProductImages();

        // ----------------------------------------------------------
        // Update url parameter
        // ----------------------------------------------------------

        this.getColorValue() && Functions.updateUrlParameter(VariantUrlToken.color, this.getColorValue().urlToken);
        this.getLensColorValue() && Functions.updateUrlParameter(VariantUrlToken.lensColor, this.getLensColorValue().urlToken);
        this.getSetValue() && Functions.updateUrlParameter(VariantUrlToken.set, this.getSetValue().urlToken);
    }

    initializeAnimations() {
        if ($(window).width() < 992) return null;

        let $animatableElements = new Array<Functions.IAnimatableElement>();

        $(".product-description .wrapper").children().each(function (i) {
            const $element = $(this);
            $animatableElements.push({
                elementToAnimate: $element,
                direction: Functions.AnimationDirectionEnum.Top,
                animationTimeout: i * 90
            } as Functions.IAnimatableElement);
        });

        $animatableElements = $animatableElements.concat([
            { elementToAnimate: $(".craftmanship .text-content"), direction: Functions.AnimationDirectionEnum.Top } as Functions.IAnimatableElement,
            { elementToAnimate: $(".features .feature-item:even .image .rellax-wrapper"), direction: Functions.AnimationDirectionEnum.TopRight } as Functions.IAnimatableElement,
            { elementToAnimate: $(".features .feature-item:even .description"), direction: Functions.AnimationDirectionEnum.TopLeft, elementToWatch: $(".features .feature-item:even .description *:first") } as Functions.IAnimatableElement,
            { elementToAnimate: $(".features .feature-item:odd .description"), direction: Functions.AnimationDirectionEnum.TopRight, elementToWatch: $(".features .feature-item:odd .description *:first") } as Functions.IAnimatableElement,
            { elementToAnimate: $(".features .feature-item:odd .image .rellax-wrapper"), direction: Functions.AnimationDirectionEnum.TopLeft } as Functions.IAnimatableElement,
            { elementToAnimate: $(".details-and-care .body"), direction: Functions.AnimationDirectionEnum.Top } as Functions.IAnimatableElement
        ]);

        if (this.getProductCategories().some(x => x.name === CategoryName.eyewear)) {
            $animatableElements = $animatableElements.concat([
                { elementToAnimate: $(".lenses .caption"), direction: Functions.AnimationDirectionEnum.Top } as Functions.IAnimatableElement,
                { elementToAnimate: $(".alternative-section .alternative-product-image"), direction: Functions.AnimationDirectionEnum.TopRight, elementToWatch: $(".alternative-section .alternative-product-description .content") } as Functions.IAnimatableElement,
                { elementToAnimate: $(".alternative-section .alternative-product-description"), direction: Functions.AnimationDirectionEnum.Top, elementToWatch: $(".alternative-section .alternative-product-description .content") } as Functions.IAnimatableElement
            ]);
        }

        Functions.animateElementsInView($animatableElements);
        return null;
    }

    initializeParallax() {
        if ($(window).width() > 992) {
            $(".rellax-wrapper").each((index, wrapper) => {
                const rellaxImage = $(wrapper).find("img");
                rellaxImage.load(() => {
                    const rellaxImageHeight = rellaxImage.height();
                    var rellax = new Rellax(`#${rellaxImage.prop("id")}`,
                        {
                            speed: this.getRellaxSpeed(rellaxImageHeight),
                            center: true
                        });

                    rellaxImage.css("top", rellaxImageHeight * -.1);
                    $(wrapper).css("height", rellaxImageHeight * .85);
                });
            });
        }
    }

    getRellaxSpeed(height: number) {
        if (height > 776) {
            return 1;
        }
        else if (height < 300) {
            return 0.3;
        }
        return (height / 776).toFixed(2);
    }

    getContentSection(name: string, fallbackValue?: string, options?: ContentSectionOptions): string {
        return sumoJS.contentSection(this.state.contentSections, name, fallbackValue, ContentSectionOptions.ConsoleLogIfMissing | options);
    }

    getContentSectionFromProductAttribute(productAttributeName: string, fallbackValue?: string, options?: ContentSectionOptions): string {
        const productAttribute = this.getProductAttributeByName(productAttributeName);
        return sumoJS.contentSectionFromProductAttribute(productAttribute, fallbackValue, ContentSectionOptions.ConsoleLogIfMissing | options);
    }

    getCountry(): Country {
        return sumoJS.get(() => this.state.country, new Country());
    }

    getCompareAtPrice(): number {
        var categories = this.getProductCategories().map(x => x.name).join(" ")
        var factor;
        if (categories.includesSubstring(CategoryName.knives, true)) {
            factor = 2;
        } else {
            factor = 3.1;
        }
        var compareAtPrice = this.getTotalWithoutSaleAtPrice() * factor;
        return Math.ceil(compareAtPrice / 10) * 10;
    }

    getProductImages(): Array<EcommerceProductImage> {
        return sumoJS.get(() =>
            this.state.cartItem.getProductImageKit().productImages.filter(x => x.name.includesSubstring("big", true)),
            new Array<EcommerceProductImage>());
    }

    getShippingBoxes(): Array<EcommerceShippingBox> {
        return sumoJS.get(() => this.state.cartItem.product.shippingBoxes, new Array<EcommerceShippingBox>());
    }

    getAvailableLenseTypes(): Array<EcommerceVariantOption> {
        const allLenseTypes = sumoJS.get(() =>
            this.getProductVariantByName(VariantName.lensType).variantOptions, new Array<EcommerceVariantOption>()
        );
        const lensColorName = this.getLensColorValue(true).name;
        return lensColorName === VariantOptionName.lensColor_Clear ?
            allLenseTypes.filter(x => !x.name.includesSubstring("sun")) :
            allLenseTypes.filter(x => x.name.includesSubstring("sun"));
    }

    getAlternativeEyewearImageUrl(): string {
        const colorUrlToken = this.getColorValue(true).urlToken;
        const lensColorUrlToken = this.getLensColorValue(true).name === VariantOptionName.lensColor_Clear
            ? VariantOptionUrlToken.lensColor_BlueGrey
            : VariantOptionUrlToken.lensColor_Clear;

        const colorVariantOption = sumoJS.get(() => this.state.cartItem.product.getProductVariantByName(VariantName.color).variantOptions.filter(x => x.urlToken === colorUrlToken))[0];
        const lensColorVariantOption = sumoJS.get(() => this.state.cartItem.product.getProductVariantByName(VariantName.lensColor).variantOptions.filter(x => x.urlToken === lensColorUrlToken))[0];

        const productImage = sumoJS.get(() => this.state.cartItem.getProductImageKit([colorVariantOption, lensColorVariantOption]).productImages
            .filter(x => x.url.includesSubstring("2-big", true))[0], new EcommerceProductImage());

        return productImage.url;
    }

    getAlternativeEyewearLensColorCount(): number {
        return sumoJS.get(() => this.getProductVariantByName(VariantName.lensColor).variantOptions.filter(x => x.name !== VariantOptionName.lensColor_Clear).length);
    }

    getStockStatus(): StockStatus {
        const productStockUnit = sumoJS.get(() => this.state.cartItem.getProductStockUnit());

        if (!productStockUnit) {
            return StockStatus.OutOfStock;
        }

        if (new Date(productStockUnit.releaseDate) < new Date()) {
            if (productStockUnit.stock > 0) {
                return StockStatus.InStock;
            } else {
                if (productStockUnit.enablePreorder) {
                    return StockStatus.OutOfStockPreorder;
                } else {
                    return StockStatus.OutOfStock;
                }
            }
        } else {
            if (productStockUnit.enablePreorder) {
                return StockStatus.ComingSoonPreorder;
            } else {
                return StockStatus.ComingSoon;
            }
        }
    }

    getStockMessage(): string {
        if (this.getStockStatus() === StockStatus.OutOfStockPreorder) {
            const contentSection = this.getContentSection(ContentSectionName.product_StickerWarning_OutOfStock_PreOrder, null, ContentSectionOptions.RenderMarkdown);
            const slices = contentSection.split("===");
            const title = slices[0];
            const bodyText = $(slices[1]).text() || "";

            const shipsOnDate = sumoJS.get(() => this.state.cartItem.getProductStockUnit().shipsOn);
            const formattedShipsOnDate = sumoJS.formatDate(shipsOnDate, (dd, mm, yyyy) => `${dd}/${mm}/${yyyy}`);

            const body = `<p>${bodyText} ${formattedShipsOnDate}.</p>`;
            return `${title} ${body}`;
        }
        else if (this.getStockStatus() === StockStatus.OutOfStock) {
            const contentSection = this.getContentSection(ContentSectionName.product_StickerWarning_OutOfStock, null, ContentSectionOptions.RenderMarkdown);
            const slices = contentSection.split("===");
            return `${slices[0]} ${slices[1]}`;
        }
        else if (this.getStockStatus() === StockStatus.ComingSoonPreorder) {
            const contentSection = this.getContentSection(ContentSectionName.product_StickerWarning_PreOrder, null, ContentSectionOptions.RenderMarkdown);

            const slices = contentSection.split("===");
            const title = slices[0];
            const bodyText = $(slices[1]).text() || "";

            const shipsOnDate = sumoJS.get(() => this.state.cartItem.getProductStockUnit().shipsOn);
            const formattedShipsOnDate = sumoJS.formatDate(shipsOnDate, (dd, mm, yyyy) => `${dd}/${mm}/${yyyy}`);

            const body = `<p>${bodyText} ${formattedShipsOnDate}.</p>`;
            return `${title} ${body}`;
        }
        return null;
    }

    getHtoStock(colorOption?: EcommerceVariantOption): number {
        return sumoJS.get(() => {
            var countryLanguageCode = this.getCountry().languageCode;
            if (countryLanguageCode === CountryLanguageCode.de_DE) {
                countryLanguageCode = CountryLanguageCode.en_GB;
            }
            const htoOption = this.getProductVariantByName(VariantName.eyewearStockBatch).variantOptions.filter(x => x.name === countryLanguageCode + VariantOptionName.eyewearStockBatch_HtoSuffix)[0];
            colorOption = colorOption || this.getColorValue();
            return this.state.cartItem.getProductStockUnit([htoOption, colorOption]).stock;
        }, 0);
    }

    addToHto(colorOption?: EcommerceVariantOption) {

        colorOption = colorOption || this.getColorValue();

        // -----------------------------------------------------------------
        // Check if HTO is full
        // -----------------------------------------------------------------

        const htoCount = sumoJS.get(() =>
            this.state.cart.getCartShippingBoxByName(ShippingBoxName.eyewearHto).cartItems.length, 0);

        if (htoCount > 2) {
            $("#modal-hto-full").modal("show");
            return;
        }

        // -----------------------------------------------------------------
        // Check if HTO contains the same design
        // -----------------------------------------------------------------

        const htoContainsSameDesign = sumoJS.get(() =>
            this.state.cart.getCartShippingBoxByName(ShippingBoxName.eyewearHto).cartItems.some(x => x.product.id === this.state.cartItem.product.id), false);

        if (htoContainsSameDesign) {
            $("#modal-hto-same-design").modal("show");
            return;
        }

        // -----------------------------------------------------------------
        // Check if this color is in stock for HTO
        // -----------------------------------------------------------------

        if (this.getHtoStock(colorOption) < 1) {
            if ($(window).width() > 992) {
                this.setState({
                    showHtoColorSelector: true,
                    showPanelColorSelector: false,
                    showPanelLensesSizeSetSelector: false
                }, () => {
                    const $actionBar = $(".action-bar");
                    const $customisePanel = $(".customise-panel");

                    if ($actionBar.hasClass("color-panel-open") || $actionBar.hasClass("lenses-size-set-panel-open")) {
                        $actionBar.removeClass("color-panel-open lenses-size-set-panel-open").addClass("hto-panel-open");
                    } else if ($actionBar.hasClass("hto-panel-open")) {
                        $customisePanel.slideUp(400, () => $actionBar.removeClass("hto-panel-open"));
                    } else {
                        $actionBar.addClass("hto-panel-open");
                        $customisePanel.slideDown();
                    }
                });
            } else {
                $("#modal-hto-out-of-stock").modal("show");
            }
            return;
        }

        // -----------------------------------------------------------------

        this.setShippingBoxByName(ShippingBoxName.eyewearHto);
        this.setVariantOptionValueByName(VariantName.eyewearStockBatch, this.getCountry().languageCode + VariantOptionName.eyewearStockBatch_HtoSuffix);
        this.setVariantOptionValueByName(VariantName.color, colorOption.name);
        this.removeCartItemVariantsForHto();

        this.addToCart();
    }

    removeCartItemVariantsForHto() {
        const cartItem = this.state.cartItem;
        cartItem.cartItemVariants = cartItem.cartItemVariants.filter(x =>
            x.variant.name === VariantName.eyewearStockBatch ||
            x.variant.name === VariantName.color ||
            x.variant.name === VariantName.lensColor
        );
        this.setState({
            cartItem
        });
    }

    updateProductImages() {

        const productImages = this.getProductImages();

        if (!productImages.length) return;

        // -----------------------------------------------------------------
        // Set cartitem thumbnail
        // -----------------------------------------------------------------

        const thumbnail = sumoJS.get(() => productImages[0].url);

        if (this.state.cartItem.image !== thumbnail) {
            const cartItem = this.state.cartItem;
            cartItem.image = thumbnail;
            this.setState({
                cartItem
            });
        };

        // -----------------------------------------------------------------
        // Update product carousel images
        // -----------------------------------------------------------------

        $(".product-carousel")
            .trigger("replace.owl.carousel", productImages.map((productImage, i) => `<div class="item" id="image-${i}" style="background-image: url(${productImage.url})"></div>`).join(""))
            .trigger("refresh.owl.carousel")
            .css({ "padding-top": 0 });


        $(".nav-item:first").addClass("selected");

        $(".nav-item").hover(function () {
            $(".product-carousel").trigger('to.owl.carousel', [$(this).index(), 300]);
            $(".nav-item").removeClass("selected");
            $(this).addClass("selected");
        });

        // ----------------------------------------------------------
        // jQuery Zoom
        // ----------------------------------------------------------

        if ($(window).width() > 992) {
            $(".product-carousel .item").each((i, e) => {
                var bg = $(e).css("background-image");
                bg = bg.replace("url(", "").replace(")", "").replace(/\"/gi, "");
                $(e).zoom({ url: bg });
            });
        }
    }

    addToCart() {
        sumoEnhancedEcommerce.addToCart([this.state.cartItem], this.state.country.iso4217CurrencySymbol);

        NProgress.configure({
            showSpinner: false,
            trickleSpeed: 50
        }).start();

        sumoJS.addToCart(this.state.cartItem, () => {
            window.location.href = sumoJS.getLocalizedUrl("/cart");
            NProgress.done();
        });
    }

    handleClickSeeMoreOptions(e) {
        const element = $(e.target);
        element.next(".more-options").slideToggle();
        this.setState({
            isShowingMoreLensTypeOptions: !this.state.isShowingMoreLensTypeOptions
        });
    }

    handleCloseInformationBarBody() {
        $(".information-bar > .body").slideUp();
    }

    handleToggleCustomisePanelColor() {
        this.setState({
            showPanelColorSelector: true,
            showPanelLensesSizeSetSelector: false,
            showHtoColorSelector: false
        }, () => {
            const $actionBar = $(".action-bar");
            const $customisePanel = $(".customise-panel");

            if ($actionBar.hasClass("lenses-size-set-panel-open") || $actionBar.hasClass("hto-panel-open")) {
                $actionBar.removeClass("lenses-size-set-panel-open hto-panel-open").addClass("color-panel-open");
            } else if ($actionBar.hasClass("color-panel-open")) {
                $actionBar.removeClass("color-panel-open");
                $customisePanel.slideUp();
            } else {
                $actionBar.addClass("color-panel-open");
                $customisePanel.slideDown();
            }
        });
    }

    handleToggleCustomisePanelLensesSizeSet() {
        this.setState({
            showPanelLensesSizeSetSelector: true,
            showPanelColorSelector: false,
            showHtoColorSelector: false
        }, () => {
            const $actionBar = $(".action-bar");
            const $customisePanel = $(".customise-panel");

            if ($actionBar.hasClass("color-panel-open") || $actionBar.hasClass("hto-panel-open")) {
                $actionBar.removeClass("color-panel-open hto-panel-open").addClass("lenses-size-set-panel-open");
            } else if ($actionBar.hasClass("lenses-size-set-panel-open")) {
                $actionBar.removeClass("lenses-size-set-panel-open");
                $customisePanel.slideUp(500);
            } else {
                $actionBar.addClass("lenses-size-set-panel-open");
                $customisePanel.slideDown(500);
            }
        });
    }

    handleSizeChartOpen() {
        $("#size-chart").modal("show");
    }

    handleToggleSizeChartMeasurementTable(e) {
        this.setState({
            showSizeGuideInchesTable: e.target.checked
        },
            () => $("#size-chart table").addClass("table"));
    }

    handleCloseCustomisePanel() {
        const $customisePanel = $(".customise-panel");
        const $actionBar = $(".action-bar");

        $customisePanel.slideUp();
        $actionBar.removeClass("color-panel-open lenses-size-set-panel-open hto-panel-open");
    }

    handleToggleStockMessage() {
        const $wrapper = $(".product-images .product-message .content-wrapper");
        $wrapper.find(".text-content p:last").slideToggle();
        $wrapper.toggleClass("shrinked");
    }

    renderPrice() {
        return (
            <span className="price">
                {
                    this.getSaleAtPrice() > 0 &&
                    <span>
                        <span className="total-after-discount">
                            {
                                this.getCountry().currencySymbol +
                                this.getTotal()
                            }
                        </span>
                        <span className="total-before-discount">
                            {
                                this.getCountry().currencySymbol +
                                this.getTotalWithoutSaleAtPrice()
                            }
                        </span>
                    </span>
                }
                {
                    this.getSaleAtPrice() === 0 &&
                    <span className="total">
                        {
                            this.getCountry().currencySymbol +
                            this.getTotal()
                        }
                    </span>
                }
                &nbsp;
                <span className="compare-at-price">
                    ({
                        this.getContentSection(ContentSectionName.product_CompareAtPrice) +
                        " " +
                        this.getCountry().currencySymbol +
                        this.getCompareAtPrice()
                    })
                </span>
            </span>
        );
    }

    renderColorSelector(variantName: string, excludeVariantOptionNames?: Array<string>, renderForHTO?: boolean) {
        const productVariant = this.getProductVariantByName(variantName, true);
        const selectedVariantOption = variantName === VariantName.color
            ? this.getColorValue(true)
            : this.getLensColorValue(true);

        return (
            <div className="color-selector">
                <div className="title">
                    {
                        selectedVariantOption.localizedTitle
                    }
                </div>
                <span className="color-icon-group">
                    {
                        productVariant.variantOptions.filter(x => excludeVariantOptionNames ? !excludeVariantOptionNames.contains(x.name) : x).map(variantOption =>
                            <span
                                key={variantOption.key}
                                title={variantOption.localizedTitle}
                                className={
                                    `${selectedVariantOption.name === variantOption.name ? "selected" : ""}
                                     ${renderForHTO && this.getHtoStock(variantOption) < 1 ? " disable-interaction" : ""}`
                                }
                                onClick={() => renderForHTO ? this.addToHto(variantOption) : this.setVariantOptionValue(variantName, variantOption)}>
                                <img src={variantOption.image} alt={variantOption.localizedTitle} />
                            </span>
                        )
                    }
                </span>
            </div>
        );
    }

    renderSizeSelector() {
        return (
            <div className="size-selector">
                <div className="selector-group">
                    {
                        this.renderDefaultSelector(VariantName.size)
                    }
                    {
                        this.getProductAttributeByName(AttributeName.contentSectionSizeChart) &&
                        <a className="size-guide" onClick={() => this.handleSizeChartOpen()}>
                            {
                                this.getContentSection(ContentSectionName.product_SizeSelector_SizeGuide)
                            }
                        </a>
                    }
                </div>
            </div>
        );
    }

    renderSizeChart() {
        const contentSection = this.getContentSectionFromProductAttribute(AttributeName.contentSectionSizeChart, null, ContentSectionOptions.RenderMarkdown);
        const slices = contentSection.split("===");

        const caption = slices[0];
        const CMTable = slices[1] || "";
        const inchesTable = slices[2] || "";
        const image = slices[3] || "";
        const bulletPoints = slices[4] || "";

        return (
            <ModalComponent {...{
                id: "size-chart",
                sizeClass: "modal-extra-lg",
                body:
                <div>
                    <div className="caption" dangerouslySetInnerHTML={{
                        __html: caption
                    }}>
                    </div>
                    <ToggleSwitchComponent{...{
                        leftLabel: "Cm",
                        rightLabel: "Inches",
                        value: this.state.showSizeGuideInchesTable,
                        handleChangeValue: (e) => this.handleToggleSizeChartMeasurementTable(e)
                    } as IToggleSwitchProps} />
                    <div className="product-title">
                        {
                            this.state.cartItem.product.localizedTitle
                        }
                    </div>
                    {
                        !this.state.showSizeGuideInchesTable &&
                        <div className="measuring-table-wrapper table-responsive cm-table-wrapper" dangerouslySetInnerHTML={{
                            __html: CMTable
                        }}>
                        </div>
                    }
                    {
                        this.state.showSizeGuideInchesTable &&
                        <div className="measuring-table-wrapper table-responsive inches-table-wrapper" dangerouslySetInnerHTML={{
                            __html: inchesTable
                        }}>
                        </div>
                    }
                    <div className="measuring-guide-title">
                        Measuring guide
                    </div>
                    <div className="display-table measuring-guide-body">
                        <div className="display-table-row">
                            <div className="display-table-cell content-cell" dangerouslySetInnerHTML={{
                                __html: bulletPoints
                            }}>
                            </div>
                            <div className="display-table-cell image-cell" dangerouslySetInnerHTML={{
                                __html: image
                            }}>
                            </div>
                        </div>
                    </div>
                    <div className="size-chart-footer">
                        <p className="title">
                            Still not sure about your size and fit?
                        </p>
                        <p>
                            Our style advisors can provide detailed information on any of our products.
                        </p>
                        <p className="email">
                            <img src="email.jpg" alt="Email" />
                            <a href={`mailto:${this.getContentSection(ContentSectionName.office_Email)}`}>
                                {
                                    this.getContentSection(ContentSectionName.office_Email)
                                }
                            </a>
                        </p>
                        <p className="phone">
                            <img src="phone.jpg" alt="Telephone" />
                            {
                                this.getContentSection(ContentSectionName.office_Phone)
                            }
                        </p>
                    </div>
                </div>
            } as IModalComponentProps} />
        );
    }

    renderSetSelector() {
        return (
            <div className="set-selector">
                <div className="selector-group">
                    {
                        this.renderDefaultSelector(VariantName.set)
                    }
                </div>
            </div>
        );
    }

    renderDefaultSelector(variantName: string) {
        return (
            <span className="selector-options-wrapper">
                {
                    this.getProductVariantByName(variantName).variantOptions.map(variantOption =>
                        <span
                            key={variantOption.key}
                            className={this.getVariantOptionValue(variantName, true).name === variantOption.name ? "selected" : ""}
                            onClick={() => this.setVariantOptionValue(variantName, variantOption)}>
                            {
                                variantOption.localizedTitle
                            }
                        </span>
                    )
                }
            </span>
        );
    }

    renderCustomisePanel() {
        const isEyeGlasses = this.getLensColorValue() && this.getLensColorValue().name === VariantOptionName.lensColor_Clear;

        return (
            <div className="customise-panel">
                <span className="close-panel" onClick={() => this.handleCloseCustomisePanel()}>Close</span>
                {
                    this.state.showPanelColorSelector &&
                    this.getColorValue() &&
                    <div className="content color">
                        <div className={`title${isEyeGlasses ? "" : " not-eyeglasses"}`} dangerouslySetInnerHTML={{
                            __html: this.getContentSection(ContentSectionName.product_ActionBar_Color_Title, null, ContentSectionOptions.RenderMarkdown)
                        }}>
                        </div>
                        <div className="body">
                            {
                                this.renderColorSelector(VariantName.color)
                            }
                        </div>
                    </div>
                }
                {
                    this.getProductCategories().some(x => x.name === CategoryName.eyewear) &&
                    this.renderEyewearPanelContent()
                }
                {
                    this.state.showPanelLensesSizeSetSelector &&
                    this.getSizeValue() &&
                    <div className="content size">
                        <div className="title">
                            {
                                this.getContentSection(ContentSectionName.product_ActionBar_Size_Title)
                            }
                        </div>
                        <div className="body">
                            {
                                this.renderSizeSelector()
                            }
                        </div>
                    </div>
                }
                {
                    this.state.showPanelLensesSizeSetSelector &&
                    this.getSetValue() &&
                    <div className="content set">
                        <div className="title">
                            {
                                this.getContentSection(ContentSectionName.product_ActionBar_Set_Title)
                            }
                        </div>
                        <div className="body">
                            {
                                this.renderSetSelector()
                            }
                        </div>
                        <div className="subtitle">
                            {
                                this.getSetValue(true).localizedDescription
                            }
                        </div>
                    </div>
                }
            </div>
        );
    }

    renderEyewearPanelContent() {
        return (
            <div className="content eyewear">
                {
                    this.state.showPanelLensesSizeSetSelector &&
                    this.renderLensTypeColumn()
                }
                {
                    this.state.showPanelLensesSizeSetSelector &&
                    this.getLensColorValue() &&
                    this.getLensColorValue().name !== VariantOptionName.lensColor_Clear &&
                    this.renderLensColorColumn()
                }
                {
                    this.state.showHtoColorSelector &&
                    this.renderHtoColorColumn()
                }
                {
                    this.getLensColorValue() &&
                    <div className="learn-more display-table">
                        <div className="display-table-row">
                            <div className="display-table-cell">
                                <img src="icon.svg" alt="Company name" />
                            </div>
                            <div className="display-table-cell">
                                <span dangerouslySetInnerHTML={{
                                    __html: this.getContentSection(ContentSectionName.product_Eyewear_LearnMore_Link, null, ContentSectionOptions.RenderMarkdown)
                                }}>
                                </span>
                            </div>
                        </div>
                    </div>
                }
            </div>
        );
    }

    renderLensTypeColumn() {
        const contentSection = this.getContentSection(ContentSectionName.product_LensType_Title);
        const slices = contentSection.split("===");

        const title = slices[0];
        const titleMobile = slices[1] || "";

        return (
            <div className="lens-type-column">
                <div className="title">
                    {
                        title
                    }
                </div>
                <div className="title mobile">
                    {
                        titleMobile
                    }
                </div>
                <div className="body">
                    <div className="display-table lens">
                        {
                            this.getAvailableLenseTypes().slice(0, 3).map((variantOption) => this.renderLensTypeInput(variantOption))
                        }
                    </div>
                    <span className="link see-more-link" onClick={(e) => this.handleClickSeeMoreOptions(e)}>
                        {
                            !this.state.isShowingMoreLensTypeOptions &&
                            this.getContentSection(ContentSectionName.product_LensType_MoreOptions)
                        }
                        {
                            this.state.isShowingMoreLensTypeOptions &&
                            this.getContentSection(ContentSectionName.product_LensType_LessOptions)
                        }
                    </span>
                    <div className="more-options">
                        <div className="display-table lens">
                            {
                                this.getAvailableLenseTypes().slice(3, 99).map((variantOption) => this.renderLensTypeInput(variantOption))
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderLensColorColumn() {
        const contentSection = this.getContentSection(ContentSectionName.product_LensColor_Title);
        const slices = contentSection.split("===");

        const title = slices[0];
        const titleMobile = slices[1] || "";

        return (
            <div className="lens-color-column">
                <div className="lens-color">
                    <div className="title">
                        {
                            title
                        }
                    </div>
                    <div className="title mobile">
                        {
                            titleMobile
                        }
                    </div>
                    <div className="body">
                        {
                            this.renderColorSelector(VariantName.lensColor, [VariantOptionName.lensColor_Clear])
                        }
                    </div>
                </div>
            </div>
        );
    }

    renderHtoColorColumn() {
        const content = this.getContentSection(ContentSectionName.product_Eyewear_Warnings_HtoOutOfStock);
        const slices = content.split("===");
        const title = slices[0];
        const subTitle = slices[1];

        return (
            <div className="color">
                <p className="title">
                    {
                        title
                    }
                </p>
                <p className="sub-title">
                    {
                        subTitle
                    }
                </p>
                <div className="body">
                    {
                        this.renderColorSelector(VariantName.color, null, true)
                    }
                </div>
            </div>
        );
    }

    renderLensTypeInput(variantOption: EcommerceVariantOption) {
        const isSelected = this.getLensTypeValue(true).name === variantOption.name;
        const currencySymbol = sumoJS.get(() => this.getCountry().currencySymbol);
        const relativePrice = this.state.cartItem.product.getTotalAfterTax(this.state.country.taxes, null, [variantOption]) - this.getTotal();
        const relativeSymbol = relativePrice === 0
            ? ""
            : relativePrice < 0
                ? "-"
                : "+";

        return (
            <div key={variantOption.key} className={`display-table-row lens-type-wrapper ${isSelected ? "selected" : ""}`}>
                <div className="display-table-cell lens-type">
                    <div className="custom-radio-button">
                        <label>
                            <input
                                type="radio"
                                value={variantOption.name}
                                checked={isSelected}
                                onChange={(e) => this.setVariantOptionValueByName(VariantName.lensType, e.target.value)} />
                            <span className="lens-type-content">
                                {
                                    variantOption.localizedTitle
                                }
                                <span className="price mobile">
                                    &nbsp;
                                    {
                                        isSelected
                                            ? "Selected"
                                            : `${relativeSymbol}${currencySymbol}${Math.abs(relativePrice)}`
                                    }
                                </span>
                            </span>
                        </label>
                    </div>
                    {
                        isSelected &&
                        <div className="lens-type-description">
                            {
                                variantOption.localizedDescription
                            }
                        </div>
                    }
                </div>
                <div className="display-table-cell price desktop">
                    {
                        isSelected
                            ? "Selected"
                            : `${relativeSymbol}${currencySymbol}${Math.abs(relativePrice)}`
                    }
                </div>
            </div>
        );
    }

    renderAlternativeEyewearSection() {
        const titleContentSection = this.getContentSection(ContentSectionName.product_Lenses_AlternativeEyewear_Title).replace(/\r?\n|\r/g, "");
        const subTitleContentSection = this.getContentSection(ContentSectionName.product_Lenses_AlternativeEyewear_SubTitle).replace(/\r?\n|\r/g, "");
        const titleSlices = titleContentSection.split("===");
        const subTitleSlices = subTitleContentSection.split("===");

        const titleSlice1 = titleSlices[0];
        const titleSlice2 = titleSlices[1] || "";
        const subTitleSlice1 = subTitleSlices[0];
        const subTitleSlice2 = subTitleSlices[1] || "";

        return (
            <div className="display-table-md alternative-section">
                <div className="display-table-row">
                    <div className="display-table-cell alternative-product-description">
                        <img src="icon.svg" className="plus" alt="alternative" />
                        <div className="content">
                            <h2 className="title">
                                {
                                    `${titleSlice1} ${sumoJS.get(() => this.state.cartItem.product.localizedTitle)} ${titleSlice2} `
                                }
                                {
                                    this.getContentSection(this.getLensColorValue(true).name === VariantOptionName.lensColor_Clear
                                        ? ContentSectionName.shared_Sunglasses
                                        : ContentSectionName.shared_Optics
                                    )
                                }
                            </h2>
                            <div className="subtitle">
                                {
                                    this.getLensColorValue(true).name === VariantOptionName.lensColor_Clear &&
                                    `${subTitleSlice1} ${this.getAlternativeEyewearLensColorCount()} ${subTitleSlice2}`
                                }
                            </div>
                            <a className="call-to-action" href={
                                sumoJS.getLocalizedUrl(`/collection/product/${sumoJS.get(() => this.state.cartItem.product.urlToken)}` +
                                    `?${VariantUrlToken.color}=${this.getColorValue(true).urlToken}` +
                                    `&${VariantUrlToken.lensColor}=${this.getLensColorValue(true).name === VariantOptionName.lensColor_Clear ? VariantOptionUrlToken.lensColor_BlueGrey : VariantOptionUrlToken.lensColor_Clear}`)}>
                                {
                                    this.getContentSection(ContentSectionName.shared_ShopNow)
                                }
                            </a>
                        </div>
                    </div>
                    <div className="display-table-cell alternative-product-image">
                        <div
                            className="image"
                            style={{ backgroundImage: `url(${this.getAlternativeEyewearImageUrl()})` }}>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderAddToCartButton() {
        if (this.getStockStatus() === StockStatus.OutOfStock) {
            return (
                <button className="btn-custom add-to-cart" disabled={true}>
                    {
                        this.getContentSection(ContentSectionName.product_OutOfStock)
                    }
                </button>
            );
        }

        return (
            <button
                className="btn-custom add-to-cart"
                onClick={() => this.addToCart()}
                disabled={
                    this.getStockStatus() === StockStatus.ComingSoon
                }>
                <span hidden={
                    this.getStockStatus() !== StockStatus.OutOfStockPreorder &&
                    this.getStockStatus() !== StockStatus.ComingSoonPreorder}>
                    {
                        this.getContentSection(ContentSectionName.shared_PreorderNow)
                    }
                </span>
                <span hidden={
                    this.getStockStatus() === StockStatus.OutOfStockPreorder ||
                    this.getStockStatus() === StockStatus.ComingSoonPreorder}>
                    {
                        this.getContentSection(ContentSectionName.product_AddToCart)
                    }
                </span>
            </button>
        );
    }

    renderProductDetailsSection() {
        return (
            <div className="product-details">
                <div className="display-table-md">
                    <div className="display-table-row">
                        <div className="display-table-cell product-image-carousel-nav">
                            {
                                this.getProductImages().map((x, i) =>
                                    <div
                                        key={x.key}
                                        data-owl=".product-carousel"
                                        data-target={i}
                                        className="nav-item"
                                        style={{ backgroundImage: `url(${x.url})` }}></div>
                                )
                            }
                        </div>
                        <div className="display-table-cell product-images">
                            <div className="product-image-carousel owl-carousel">
                                <div className="product-carousel">
                                </div>
                            </div>
                            {
                                this.getStockMessage() &&
                                <div className="product-message">
                                    <div className="content-wrapper">
                                        <img src="icon.svg" onClick={() => this.handleToggleStockMessage()} />
                                        <div className="text-content" dangerouslySetInnerHTML={{
                                            __html: this.getStockMessage()
                                        }}>
                                        </div>
                                    </div>
                                </div>
                            }
                        </div>
                        <div className="display-table-cell product-description">
                            <div className="wrapper">
                                <h2 className="title">
                                    {
                                        sumoJS.get(() => this.state.cartItem.product.localizedTitle)
                                    }
                                    &nbsp;
                                    {
                                        this.getProductCategories().some(x => x.name === CategoryName.eyewear) &&
                                        this.getContentSection(this.getLensColorValue(true).name === VariantOptionName.lensColor_Clear ? ContentSectionName.shared_Optics : ContentSectionName.shared_Sunglasses)
                                    }
                                </h2>
                                {
                                    this.renderPrice()
                                }
                                <span className="set">
                                    {
                                        this.getSetValue() &&
                                        this.getSetValue(true).localizedTitle
                                    }
                                </span>
                                {
                                    this.getColorValue() &&
                                    <div>
                                        {
                                            this.renderColorSelector(VariantName.color)
                                        }
                                    </div>
                                }
                                {
                                    this.getSizeValue() &&
                                    this.renderSizeSelector()
                                }
                                {
                                    this.getSetValue() &&
                                    this.renderSetSelector()
                                }
                                {
                                    this.getProductCategories().some(x => x.name === CategoryName.eyewear) &&
                                    this.renderEyewearPanelContent()
                                }
                                <div className="description">
                                    {
                                        sumoJS.get(() => this.state.cartItem.product.localizedDescription)
                                    }
                                </div>
                                <div className="social-share">
                                    <a href={`http://www.facebook.com/share.php?u=${window.location.href}`} target="_blank"><i className="fa fa-facebook"></i></a>
                                    <a href={`http://twitter.com/home?status=${window.location.href}`} target="_blank"><i className="fa fa-twitter"></i></a>
                                    <a href={`http://pinterest.com/pin/create/link/?url=${window.location.href}&media=${this.state.cartItem.image}&description=${this.state.cartItem.product.localizedDescription}`} target="_blank"><i className="fa fa-pinterest"></i></a>
                                    <a href={`https://plus.google.com/share?url=${window.location.href}`} target="_blank"><i className="fa fa-google-plus"></i></a>
                                </div>
                                <div>
                                    {
                                        this.getStockMessage() &&
                                        <div className="product-message" dangerouslySetInnerHTML={{
                                            __html: this.getStockMessage()
                                        }}>
                                        </div>
                                    }
                                </div>
                                <div className="call-to-action-buttons">
                                    {
                                        this.renderAddToCartButton()
                                    }
                                    {
                                        this.getProductCategories().some(x => x.name === CategoryName.eyewear) &&
                                        this.getLensColorValue(true).name === VariantOptionName.lensColor_Clear &&
                                        <button className="btn-custom try" onClick={() => this.addToHto()}>
                                            <i className="fa fa-plus"></i>
                                            {
                                                this.getContentSection(ContentSectionName.product_TryBeforeYouBuy)
                                            }
                                        </button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderInformationBarSection() {
        const needHelpBodyContentSection = this.getContentSection(ContentSectionName.product_InformationBar_NeedHelp_Body);
        const needHelpBodySlices = needHelpBodyContentSection.split("===");
        const needHelpBodyTitle = needHelpBodySlices[0];
        const needHelpBodyBody = needHelpBodySlices[1] || "";

        return (
            <div className="information-bar">
                <div className="header">
                    <div className="container-responsive-xl">
                        <div className="display-table">
                            <div className="display-table-row">
                                <div className="display-table-cell">
                                    <div className="item" data-target="delivery">
                                        <span className="title">
                                            {
                                                this.getContentSection(ContentSectionName.product_InformationBar_Delivery_Title)
                                            }
                                        </span>
                                    </div>
                                </div>
                                <div className="display-table-cell">
                                    <div className="item" data-target="returns">
                                        <img className="image" src="returns.svg" alt="Returns" />
                                        <span className="title">
                                            {
                                                this.getContentSection(ContentSectionName.product_InformationBar_Returns_Title)
                                            }
                                        </span>
                                    </div>
                                </div>
                                <div className="display-table-cell">
                                    <div className="item" data-target="need-help">
                                        <span className="title">
                                            {
                                                this.getContentSection(ContentSectionName.product_InformationBar_NeedHelp_Title)
                                            }
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="body hidden">
                    <div className="content delivery">
                        <div className="display-table-md">
                            <div className="display-table-row">
                                {
                                    this.getShippingBoxes().map(shippingBox =>
                                        <div key={shippingBox.key} className="display-table-cell item">
                                            <div className="name">
                                                {
                                                    shippingBox.localizedTitle
                                                }
                                            </div>
                                            <div className="days">
                                                {
                                                    `${shippingBox.localizedMinDays} - ${shippingBox.localizedMaxDays} ${this.getContentSection(ContentSectionName.shared_WorkingDays)}`
                                                }
                                            </div>
                                            <div className="description">
                                                {
                                                    shippingBox.localizedDescription
                                                }
                                            </div>
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                    </div>
                    <div className="content returns">
                        <div className="title">
                            {
                                this.getContentSection(ContentSectionName.product_InformationBar_ReturnsPolicy)
                            }
                        </div>
                        <div
                            className="body"
                            dangerouslySetInnerHTML={{
                                __html: this.getContentSectionFromProductAttribute(AttributeName.contentSectionReturnsPolicy, null, ContentSectionOptions.RenderMarkdown)
                            }}>
                        </div>
                    </div>
                    <div className="content need-help">
                        <div className="title">
                            {
                                needHelpBodyTitle
                            }
                        </div>
                        <div className="body">
                            <p>
                                {
                                    needHelpBodyBody
                                }
                            </p>
                            <div className="display-table-md">
                                <div className="display-table-row">
                                    <div className="display-table-cell contact-details">
                                        <div className="display-table">
                                            <div className="display-table-row email">
                                                <div className="display-table-cell icon">
                                                    <img src="email.svg" alt="Email" />
                                                </div>
                                                <div className="display-table-cell content">
                                                    <a className="email-link" href={`mailto:${this.getContentSection(ContentSectionName.office_Email)}`}>
                                                        {
                                                            this.getContentSection(ContentSectionName.office_Email)
                                                        }
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="display-table-row phone-info">
                                                <div className="display-table-cell icon">
                                                </div>
                                                <div className="display-table-cell content">
                                                    <div className="phone-number">
                                                        {
                                                            this.getContentSection(ContentSectionName.office_Phone)
                                                        }
                                                    </div>
                                                    <div className="business-hours">
                                                        <div className="description">
                                                            {
                                                                this.getContentSection(ContentSectionName.product_InformationBar_NeedHelp_BusinessHours)
                                                            }
                                                        </div>
                                                        <div className="value">
                                                            {
                                                                this.getContentSection(ContentSectionName.office_WorkingHours)
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="display-table-row help-faq">
                                                <div className="display-table-cell icon">
                                                </div>
                                                <div className="display-table-cell content" dangerouslySetInnerHTML={{
                                                    __html: this.getContentSection(ContentSectionName.product_InformationBar_NeedHelp_Faq_Link, null, ContentSectionOptions.RenderMarkdown)
                                                }}>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="display-table-cell contact-form-wrapper">
                                        <div className="contact-form">
                                            <ContactFormComponent />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="body-footer">
                        <span onClick={() => this.handleCloseInformationBarBody()}>
                            {
                                this.getContentSection(ContentSectionName.product_InformationBar_Close)
                            }
                        </span>
                    </div>
                </div>
            </div>
        );
    }

    renderCraftsmanshipSection() {
        const contentSection = this.getContentSectionFromProductAttribute(AttributeName.contentSectionCraftsmanship, null, ContentSectionOptions.RenderMarkdown | ContentSectionOptions.AllowHtml);
        const slices = contentSection.split("===");
        const caption = slices[0];
        const workshopUrl = $(slices[1]).prop("href") || "";
        const workshopCallToAction = $(slices[1]).text() || "";
        const mediaSlice = slices[2] || "";
        const mediaUrl = $(mediaSlice).prop("src") || "";
        const isVideo = mediaSlice.includesSubstring("video", true);

        return (
            <section className="craftmanship">
                <div className="caption">
                    <div className="text-content">
                        <div dangerouslySetInnerHTML={{
                            __html: caption
                        }}>
                        </div>
                        <a className="workshop" href={workshopUrl}>
                            {
                                workshopCallToAction
                            }
                        </a>
                    </div>
                </div>
                <div className="media-cover">
                    {
                        isVideo
                            ? <video
                                id="product-craftmanship-video"
                                className="product-craftmanship-video"
                                src={mediaUrl}
                                autoPlay
                                loop>
                            </video>
                            : <div
                                className="product-craftmanship-image"
                                style={{ backgroundImage: `url(${mediaUrl})` }}>
                            </div>
                    }
                </div>
            </section>
        );
    }

    renderFeaturesSection() {
        const contentSectionMaterials = this.getContentSectionFromProductAttribute(AttributeName.contentSectionMaterial, null, ContentSectionOptions.RenderMarkdown);
        const contentSectionMaterialsSlices = contentSectionMaterials.split("===");
        const contentSectionMaterialsImage = $(contentSectionMaterialsSlices[0]).prop("src");
        const contentSectionMaterialsCaption = contentSectionMaterialsSlices[1] || "";

        const contentSectionQuality = this.getContentSectionFromProductAttribute(AttributeName.contentSectionQuality, null, ContentSectionOptions.RenderMarkdown);
        const contentSectionQualitySlices = contentSectionQuality.split("===");
        const contentSectionQualityImage = $(contentSectionQualitySlices[0]).prop("src");
        const contentSectionQualityCaption = contentSectionQualitySlices[1] || "";

        return (
            <section className="features">
                <div className="display-table-md feature-item">
                    <div className="display-table-row">
                        <div className="display-table-cell description" dangerouslySetInnerHTML={{
                            __html: contentSectionMaterialsCaption
                        }}>
                        </div>
                        <div className="display-table-cell image">
                            <div className="rellax-wrapper">
                                <img src={contentSectionMaterialsImage} alt="Product Material" id="materials-image" />
                            </div>
                        </div>
                    </div>
                </div>
                <div className="feature-item display-table-md">
                    <div className="display-table-row">
                        <div className="display-table-cell description" dangerouslySetInnerHTML={{
                            __html: contentSectionQualityCaption
                        }}>
                        </div>
                        <div className="display-table-cell image">
                            <div className="rellax-wrapper">
                                <img src={contentSectionQualityImage} alt="Product Quality" id="quality-image" />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }

    renderLensesSection() {
        return (
            <section className="lenses">
                <div className="caption" dangerouslySetInnerHTML={{
                    __html: this.getContentSection(ContentSectionName.product_Lenses_Caption, null, ContentSectionOptions.RenderMarkdown)
                }}>
                </div>
                {
                    this.renderAlternativeEyewearSection()
                }
                <div className="reglazing-call-to-action" dangerouslySetInnerHTML={{
                    __html: this.getContentSection(ContentSectionName.product_Lenses_Reglazing_Link, null, ContentSectionOptions.RenderMarkdown)
                }}>
                </div>
            </section>
        );
    }

    renderSpecsSection() {
        return (
            <section className="specs">
                <div className="display-table-md">
                    <div className="display-table-row">
                        {
                            this.getProductAttributeByName(AttributeName.imageBlueprint) &&
                            <div className="display-table-cell blueprint-image">
                                <img src={
                                    this.getProductAttributeByName(AttributeName.imageBlueprint).imageValue
                                } alt="Blueprint" />
                            </div>
                        }
                        <div className="display-table-cell details-and-care">
                            <h2>
                                {
                                    this.getContentSection(ContentSectionName.product_Specs_DetailsAndCare)
                                }
                            </h2>
                            <div className="body">
                                <div
                                    className="panel-body"
                                    dangerouslySetInnerHTML={{
                                        __html: this.getContentSectionFromProductAttribute(AttributeName.contentSectionDetailsAndCare, null, ContentSectionOptions.RenderMarkdown)
                                    }}>
                                </div>
                            </div>
                            {
                                this.getProductCategories().some(x => x.name === CategoryName.eyewear) &&
                                <div className="need-help-message">
                                    <div className="content" dangerouslySetInnerHTML={{
                                        __html: this.getContentSection(ContentSectionName.product_Specs_NeedHelp_Caption, null, ContentSectionOptions.RenderMarkdown)
                                    }}>
                                    </div>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </section>
        );
    }

    renderActionBarSection() {
        return (
            <div className="action-bar">
                <div className="action-bar-controls">
                    <div className="display-table-md">
                        <div className="display-table-row">
                            <div className="display-table-cell left">
                                {
                                    this.getColorValue() &&
                                    <span className="action-bar-color" onClick={() => this.handleToggleCustomisePanelColor()}>
                                        {
                                            `${this.getContentSection(ContentSectionName.product_ActionBar_Color)}: ${this.getColorValue(true).localizedTitle}`
                                        }
                                    </span>
                                }
                                {
                                    (this.getProductCategories().some(x => x.name === CategoryName.eyewear) || this.getSizeValue() || this.getSetValue()) &&
                                    <span className="action-bar-option" onClick={() => this.handleToggleCustomisePanelLensesSizeSet()}>
                                        {
                                            this.getProductCategories().some(x => x.name === CategoryName.eyewear) &&
                                            `${this.getContentSection(ContentSectionName.shared_Lenses)}: ${this.getLensTypeValue(true).localizedTitle}, ${this.getLensColorValue(true).localizedTitle}`
                                        }
                                        {
                                            this.getSizeValue() &&
                                            `${this.getContentSection(ContentSectionName.product_ActionBar_Size)}: ${this.getSizeValue(true).localizedTitle}`
                                        }
                                        {
                                            this.getSetValue() &&
                                            `${this.getContentSection(ContentSectionName.product_ActionBar_Set)}: ${this.getSetValue(true).localizedTitle}`
                                        }
                                    </span>
                                }
                            </div>
                            <div className="display-table-cell right">
                                {
                                    this.renderPrice()
                                }
                                {
                                    this.getProductCategories().some(x => x.name === CategoryName.eyewear) &&
                                    this.getLensColorValue(true).name === VariantOptionName.lensColor_Clear &&
                                    <button className="btn-custom try" onClick={() => this.addToHto()}>
                                        <i className="fa fa-plus"></i>
                                        {
                                            this.getContentSection(ContentSectionName.product_TryBeforeYouBuy)
                                        }
                                    </button>
                                }
                                {
                                    this.renderAddToCartButton()
                                }
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.renderCustomisePanel()
                }
            </div>
        );
    }

    renderEyewearModals() {
        return (
            <div>
                <ModalComponent {...{
                    id: "modal-hto-full",
                    title: this.getContentSection(ContentSectionName.product_Modals_Title),
                    body:
                    <div dangerouslySetInnerHTML={{
                        __html: this.getContentSection(ContentSectionName.product_Eyewear_Warnings_HtoFull, null, ContentSectionOptions.RenderMarkdown)
                    }}>
                    </div>,
                    footer:
                    <div>
                        <a className="btn-custom-transparent-gray" href={sumoJS.getLocalizedUrl(Url.cart)}>
                            {
                                this.getContentSection(ContentSectionName.product_Modals_ReviewCart)
                            }
                        </a>
                        <button type="button" className="btn-custom-transparent-gray" data-dismiss="modal">
                            {
                                this.getContentSection(ContentSectionName.product_Modals_Close)
                            }
                        </button>
                    </div>
                } as IModalComponentProps} />
                <ModalComponent {...{
                    id: "modal-hto-same-design",
                    title: this.getContentSection(ContentSectionName.product_Modals_Title),
                    body:
                    <div dangerouslySetInnerHTML={{
                        __html: this.getContentSection(ContentSectionName.product_Eyewear_Warnings_HtoContainsSameDesign, null, ContentSectionOptions.RenderMarkdown)
                    }}>
                    </div>,
                    footer:
                    <div>
                        <a className="btn-custom-transparent-gray" href={sumoJS.getLocalizedUrl(Url.cart)}>
                            {
                                this.getContentSection(ContentSectionName.product_Modals_ReviewCart)
                            }
                        </a>
                        <a className="btn-custom-transparent-gray" href={sumoJS.getLocalizedUrl(Url.collectionEyewear)}>
                            {
                                this.getContentSection(ContentSectionName.product_Modals_BrowseTheCollection)
                            }
                        </a>
                    </div>
                } as IModalComponentProps} />
                <ModalComponent {...{
                    id: "modal-hto-out-of-stock",
                    sizeClass: "modal-lg",
                    title: this.getContentSection(ContentSectionName.product_Modals_Title),
                    body:
                    <div>
                        <div>
                            {
                                this.getContentSection(ContentSectionName.product_Eyewear_Warnings_HtoOutOfStock).split("===").join(" ")
                            }
                        </div>
                        <br />
                        <div className="row">
                            {
                                this.getProductVariantByName(VariantName.color).variantOptions.map(colorOption => {
                                    var isOutOfStock = this.getHtoStock(colorOption) < 1;
                                    var imageUrl = this.state.cartItem.getProductImageKit([colorOption, this.getLensColorValue()]).getProductImageByName(ProductImageName.image1Small).url;
                                    return (
                                        <div className="col-md-4" key={colorOption.key}>
                                            <div
                                                className={isOutOfStock ? "hto-thumbnail disable-interaction" : "hto-thumbnail"}
                                                onClick={() => this.addToHto(colorOption)}>
                                                <img src={imageUrl} alt="" />
                                                <p>
                                                    {
                                                        colorOption.localizedTitle + (isOutOfStock ? " " + "(Out of stock)" : "")
                                                    }
                                                </p>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>,
                    footer:
                    <div>
                        <button type="button" className="btn-custom-transparent-gray" data-dismiss="modal">
                            {
                                this.getContentSection(ContentSectionName.product_Modals_Close)
                            }
                        </button>
                    </div>
                } as IModalComponentProps} />
            </div>
        );
    }

    render() {
        if (!this.state.cartItem) {
            return <img className="spinner" src="/Content/img/icons/loading.gif" alt="" />;
        }
        return (
            <div className="product-page-component">
                {
                    this.renderProductDetailsSection()
                }
                {
                    this.renderActionBarSection()
                }
                {
                    this.renderInformationBarSection()
                }
                {
                    this.renderCraftsmanshipSection()
                }
                {
                    this.renderFeaturesSection()
                }
                {
                    this.getProductCategories().some(x => x.name === CategoryName.eyewear) &&
                    this.renderLensesSection()
                }
                {
                    this.renderSpecsSection()
                }
                {
                    this.getProductCategories().some(x => x.name === CategoryName.eyewear) &&
                    this.renderEyewearModals()
                }
                {
                    this.getProductAttributeByName(AttributeName.contentSectionSizeChart) &&
                    this.renderSizeChart()
                }
            </div>
        );
    }

}