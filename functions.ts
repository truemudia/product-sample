﻿import { IFormResponse } from "../interfaces/IFormResponse";

export namespace Functions {

    export enum AnimationDirectionEnum {
        Top,
        TopLeft,
        TopRight
    }

    export interface IAnimatableElement {
        elementToAnimate: JQuery;
        direction: AnimationDirectionEnum;
        elementToWatch?: JQuery;
        animationTimeout?: number;
    }

    // -------------------------------------------------------------------------------------------------
    /** Generates a new Guid */

    export function newGuid(): string {
        return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
            const r = Math.random() * 16 | 0;
            const v = c === "x" ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    // ------------------------------------------------------
    // Update url parameter
    // ------------------------------------------------------

    export function updateUrlParameter(param: string, value: string) {
        const regExp = new RegExp(param + "(.+?)(&|$)", "g");
        const newUrl = window.location.href.replace(regExp, param + "=" + value + "$2");
        if (newUrl !== window.location.href) {
            window.history.replaceState({}, value, newUrl);
        }
    }

    // -------------------------------------------------------------------------------------------------
    /** Submits the model with ajax. Returns an IFormResponse */

    export function ajaxSubmit(model: any, url: string, success?: (formResponse: IFormResponse) => void) {
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(model),
            contentType: "application/json",
            success(response: string) {
                success && success(JSON.parse(response));
            }
        });
    }

    export function animateElementsInView(elements: Array<IAnimatableElement>) {
        const prepForAnimation = (element: IAnimatableElement) => {
            const elementToAnimate = element.elementToAnimate;
            const direction = element.direction;

            if (direction === AnimationDirectionEnum.Top) {
                elementToAnimate.css({ "transform": "translateY(50px)", "opacity": "0" });
            }
            else if (direction === AnimationDirectionEnum.TopLeft) {
                elementToAnimate.css({ "transform": "translate(50px, 50px)", "opacity": "0" });
            }
            else if (direction === AnimationDirectionEnum.TopRight) {
                elementToAnimate.css({ "transform": "translate(-50px, 50px)", "opacity": "0" });
            }
        };

        const animate = ($element: JQuery) => {
            $element.css({ "transform": "none", "opacity": "1", "transition": "transform 1s, opacity 1s" });
        };

        const watch = (element: IAnimatableElement) => {
            const $elementToAnimate = element.elementToAnimate;
            const animationTimeout = element.animationTimeout;

            var inview = new window.Waypoint.Inview({
                element: element.elementToWatch || $elementToAnimate,
                enter: (direction) => {
                    if (direction === "down") {
                        animationTimeout
                            ? setTimeout(() => animate($elementToAnimate), animationTimeout)
                            : animate($elementToAnimate);
                    }
                },
                exited: (direction) => {
                    if (direction === "up") {
                        prepForAnimation(element);
                    }
                }
            });
        };

        elements.forEach(element => {
            prepForAnimation(element);
            watch(element);
        });
    }
}